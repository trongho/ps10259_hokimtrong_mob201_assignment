package com.example.ps10259_mob201_courses_manager_assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.ps10259_mob201_courses_manager_assignment.dao.StudentDAO;
import com.example.ps10259_mob201_courses_manager_assignment.model.Student;
import com.example.ps10259_mob201_courses_manager_assignment.service.CourseRegisterService;
import com.google.android.material.textfield.TextInputEditText;

public class EditStudentActivity extends AppCompatActivity {
    TextInputEditText edtStudentCode, edtStudentName, edtPassword, edtRePassword;
    Button btnEdit, btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_student);
        setTitle("Edit student");

        initComponent();
        getData();
        editStudent();
        huy();
    }

    public void initComponent() {
        btnEdit = findViewById(R.id.btnEdit);
        btnCancel = findViewById(R.id.btnCancel);
        edtStudentCode = findViewById(R.id.edtStudentCode);
        edtPassword = findViewById(R.id.edtPassword);
        edtRePassword = findViewById(R.id.edtRePassword);
        edtStudentName = findViewById(R.id.edtStudentName);
        edtStudentCode.setEnabled(false);
    }

    public void getData(){
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("edit_student");
        edtStudentCode.setText(bundle.getString("student_code"));
        edtStudentName.setText(bundle.getString("student_name"));
        edtPassword.setText(bundle.getString("password"));
        edtRePassword.setText(bundle.getString("password"));
    }

    public void editStudent(){
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validate()>0) {
                    StudentDAO studentDAO = new StudentDAO(EditStudentActivity.this);
                    Student student = new Student();
                    student.setStudentCode(edtStudentCode.getText().toString());
                    student.setStudentName(edtStudentName.getText().toString());
                    student.setPassword(edtPassword.getText().toString());

                    if (studentDAO.update(student) == -1) {
                        Toast.makeText(EditStudentActivity.this, "Edit fail!!!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(EditStudentActivity.this, "Edit success!!!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }

            }
        });
    }

    public int validate(){
        int check=1;
        //validate data
        if (edtStudentCode.length() == 0) {
            Toast.makeText(EditStudentActivity.this, "Student code is not empty!!!", Toast.LENGTH_SHORT).show();
            check=-1;
        }
        if (edtStudentName.length() == 0) {
            Toast.makeText(EditStudentActivity.this, "Student name is not empty!!!", Toast.LENGTH_SHORT).show();
            check=-1;
        }
        if (edtPassword.length() == 0) {
            Toast.makeText(EditStudentActivity.this, "Password is not empty!!!", Toast.LENGTH_SHORT).show();
            check=-1;
        }
        if (edtRePassword.length() == 0) {
            Toast.makeText(EditStudentActivity.this, "RePassword is not empty!!!", Toast.LENGTH_SHORT).show();
            check=-1;
        }
        if (!edtRePassword.getText().toString().equals(edtPassword.getText().toString())) {
            Toast.makeText(EditStudentActivity.this, "RePassword is equals password!!!", Toast.LENGTH_SHORT).show();
            check=-1;
        }
        return check;
    }

    public void huy(){
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
