package com.example.ps10259_mob201_courses_manager_assignment.model;

public class Student {
    String studentCode,studentName,password;

    public Student() {
    }

    public Student(String studentCode, String studentName,String password) {
        this.studentCode = studentCode;
        this.studentName = studentName;
        this.password=password;
    }

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
