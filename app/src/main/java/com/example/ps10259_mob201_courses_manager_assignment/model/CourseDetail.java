package com.example.ps10259_mob201_courses_manager_assignment.model;

public class CourseDetail {
    int courseDetailCode;
    String courseCode,studentCode;

    public CourseDetail() {
    }

    public CourseDetail(int courseDetailCode,String courseCode, String studentCode) {
        this.courseDetailCode=courseDetailCode;
        this.courseCode = courseCode;
        this.studentCode = studentCode;
    }

    public int getCourseDetailCode() {
        return courseDetailCode;
    }

    public void setCourseDetailCode(int courseDetailCode) {
        this.courseDetailCode = courseDetailCode;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }
}
