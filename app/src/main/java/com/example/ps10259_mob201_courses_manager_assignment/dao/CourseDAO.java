package com.example.ps10259_mob201_courses_manager_assignment.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ps10259_mob201_courses_manager_assignment.database.DBHelper;
import com.example.ps10259_mob201_courses_manager_assignment.model.Course;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CourseDAO {
    DBHelper dbHelper;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public CourseDAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    public ArrayList<Course> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<Course> list = new ArrayList<>();
        String sql = "SELECT * FROM COURSE";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String courseCode = c.getString(0);
                String courseName = c.getString(1);
                String teacherName = c.getString(2);
                int creditNumber = c.getInt(3);
                Date startDate = simpleDateFormat.parse(c.getString(4));
                Date endDate = simpleDateFormat.parse(c.getString(5));
                list.add(new Course(courseCode, courseName, teacherName, creditNumber, startDate, endDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }


    public long insert(Course course) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("COURSE_CODE", course.getCourseCode());
        values.put("COURSE_NAME", course.getCourseName());
        values.put("TEACHER_NAME", course.getTeacherName());
        values.put("CREDIT_NUMBER", course.getCreditNumber());
        values.put("START_DATE", simpleDateFormat.format(course.getStartDate()));
        values.put("END_DATE", simpleDateFormat.format(course.getEndDate()));
        return db.insert("COURSE", null, values);
    }

    public int update(Course course) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("COURSE_CODE", course.getCourseCode());
        values.put("COURSE_NAME", course.getCourseName());
        values.put("TEACHER_NAME", course.getTeacherName());
        values.put("CREDIT_NUMBER", course.getCreditNumber());
        values.put("START_DATE", course.getStartDate().toString());
        values.put("END_DATE", course.getEndDate().toString());
        return db.update("COURSE", values, "COURSE_CODE=?", new String[]{course.getCourseCode()});
    }

    public int delete(Course course) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("COURSE", "COURSE_CODE=?", new String[]{course.getCourseCode()});
    }

}
