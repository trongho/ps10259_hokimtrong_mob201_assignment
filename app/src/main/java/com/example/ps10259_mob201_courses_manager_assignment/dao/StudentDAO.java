package com.example.ps10259_mob201_courses_manager_assignment.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ps10259_mob201_courses_manager_assignment.database.DBHelper;
import com.example.ps10259_mob201_courses_manager_assignment.model.Student;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

public class StudentDAO {
    DBHelper dbHelper;

    public StudentDAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    public ArrayList<Student> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<Student> list = new ArrayList<>();
        String sql = "SELECT * FROM STUDENT";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
                String studentCode = c.getString(0);
                String studentName = c.getString(1);
                String password = c.getString(2);
                list.add(new Student(studentCode,studentName,password));
            c.moveToNext();
        }
        return list;
    }

    public Student getStudentByID(String sCode){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Student student=new Student();
        String sql = "SELECT * FROM STUDENT WHERE STUDENT_CODE=?";
        Cursor c = db.rawQuery(sql, new String[]{sCode});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            String studentCode = c.getString(0);
            String studentName = c.getString(1);
            String password = c.getString(2);
            student=new Student(studentCode,studentName,password);
            c.moveToNext();
        }
        return student;
    }


    public long insert(Student student) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("STUDENT_CODE", student.getStudentCode());
        values.put("STUDENT_NAME", student.getStudentName());
        values.put("PASSWORD", student.getPassword());
        return db.insert("STUDENT", null, values);
    }

    public int update(Student student) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("STUDENT_CODE", student.getStudentCode());
        values.put("STUDENT_NAME", student.getStudentName());
        values.put("PASSWORD", student.getPassword());
        return db.update("STUDENT", values, "STUDENT_CODE=?", new String[]{student.getStudentCode()});
    }

    public int delete(Student student) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("STUDENT", "STUDENT_CODE=?", new String[]{student.getStudentCode()});
    }

    public boolean checkLogin(String sCode,String pw){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        boolean flag=false;
        String sql = "SELECT STUDENT_CODE,PASSWORD FROM STUDENT WHERE STUDENT_CODE=? AND PASSWORD=?";
        Cursor c = db.rawQuery(sql, new String[]{sCode,pw});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            Student student=new Student();
            String studentCode = c.getString(0);
            String password = c.getString(1);
            if(studentCode.equals(sCode)&&password.equals(pw)) {
                flag=true;
                break;
            }
            c.moveToNext();
        }
        return flag;
    }

}
