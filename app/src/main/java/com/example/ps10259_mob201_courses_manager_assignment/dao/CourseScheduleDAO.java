package com.example.ps10259_mob201_courses_manager_assignment.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ps10259_mob201_courses_manager_assignment.database.DBHelper;
import com.example.ps10259_mob201_courses_manager_assignment.model.CourseDetail;
import com.example.ps10259_mob201_courses_manager_assignment.model.CourseSchedule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CourseScheduleDAO {
    DBHelper dbHelper;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

    public CourseScheduleDAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    public ArrayList<CourseSchedule> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<CourseSchedule> list = new ArrayList<>();
        String sql = "SELECT * FROM COURSE_SCHEDULE JOIN COURSE ON COURSE_SCHEDULE.COURSE_CODE=COURSE.COURSE_CODE GROUP BY COURSE_SCHEDULE_CODE ORDER BY COURSE_CODE";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int courseScheduleCode = c.getInt(0);
                String courseCode = c.getString(1);
                String courseContent = c.getString(2);
                String room = c.getString(3);
                Date studyDate = simpleDateFormat.parse(c.getString(4));
                list.add(new CourseSchedule(courseScheduleCode, courseCode, courseContent,room,studyDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<CourseSchedule> getCourseScheduleByStudentCode(String sCode) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<CourseSchedule> list = new ArrayList<>();
        String sql = "SELECT * FROM COURSE_SCHEDULE,COURSE_DETAIL ON COURSE_SCHEDULE.COURSE_CODE=COURSE_DETAIL.COURSE_CODE WHERE COURSE_DETAIL.STUDENT_CODE=? ";
        Cursor c = db.rawQuery(sql, new String[]{sCode});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int courseScheduleCode = c.getInt(0);
                String courseCode = c.getString(1);
                String courseContent = c.getString(2);
                String room = c.getString(3);
                Date studyDate = simpleDateFormat.parse(c.getString(4));
                list.add(new CourseSchedule(courseScheduleCode, courseCode, courseContent,room,studyDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }


    public long insert(CourseSchedule courseSchedule) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("COURSE_SCHEDULE_CODE", courseSchedule.getCourseScheduleCode());
        values.put("COURSE_CODE", courseSchedule.getCourseCode());
        values.put("COURSE_CONTENT", courseSchedule.getCourseContent());
        values.put("ROOM", courseSchedule.getRoom());
        values.put("STUDY_DATE", simpleDateFormat.format(courseSchedule.getStudyDate()));
        return db.insert("COURSE_SCHEDULE", null, values);
    }

    public int update(CourseSchedule courseSchedule) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("COURSE_SCHEDULE_CODE", courseSchedule.getCourseScheduleCode());
        values.put("COURSE_CODE", courseSchedule.getCourseCode());
        values.put("COURSE_CONTENT", courseSchedule.getCourseContent());
        values.put("ROOM", courseSchedule.getRoom());
        values.put("STUDY_DATE", simpleDateFormat.format(courseSchedule.getStudyDate()));
        return db.update("COURSE_SCHEDULE", values, "COURSE_SCHEDULE_CODE=?", new String[]{String.valueOf(courseSchedule.getCourseScheduleCode())});
    }

    public int delete(CourseSchedule courseSchedule) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("COURSE_SCHEDULE", "COURSE_SCHEDULE_CODE=?", new String[]{String.valueOf(courseSchedule.getCourseScheduleCode())});
    }
}
