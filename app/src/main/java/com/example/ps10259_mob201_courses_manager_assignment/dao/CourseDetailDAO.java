package com.example.ps10259_mob201_courses_manager_assignment.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ps10259_mob201_courses_manager_assignment.database.DBHelper;
import com.example.ps10259_mob201_courses_manager_assignment.fragment.MyCourseFragment;
import com.example.ps10259_mob201_courses_manager_assignment.model.CourseDetail;
import com.example.ps10259_mob201_courses_manager_assignment.model.Student;

import java.util.ArrayList;

public class CourseDetailDAO {
    DBHelper dbHelper;

    public CourseDetailDAO(Context context) {
        dbHelper = new DBHelper(context);
    }


    public ArrayList<CourseDetail> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<CourseDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM COURSE_DETAIL " +
                "INNER JOIN COURSE ON COURSE_DETAIL.COURSE_CODE=COURSE.COURSE_CODE " +
                "INNER JOIN STUDENT ON COURSE_DETAIL.STUDENT_CODE=STUDENT.STUDENT_CODE GROUP BY COURSE_DETAIL.COURSE_CODE";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            int courseDetailCode = c.getInt(0);
            String courseCode = c.getString(1);
            String studentCode = c.getString(2);
            list.add(new CourseDetail(courseDetailCode,courseCode,studentCode));
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<CourseDetail> getCourseByStudentCode(String sCode) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<CourseDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM COURSE_DETAIL JOIN STUDENT ON COURSE_DETAIL.STUDENT_CODE=STUDENT.STUDENT_CODE WHERE COURSE_DETAIL.STUDENT_CODE=? GROUP BY COURSE_DETAIL.COURSE_CODE";
        Cursor c = db.rawQuery(sql, new String[]{sCode});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            int courseDetailCode = c.getInt(0);
            String courseCode = c.getString(1);
            String studentCode = c.getString(2);
            list.add(new CourseDetail(courseDetailCode,courseCode,studentCode));
            c.moveToNext();
        }
        return list;
    }

    public long insert(CourseDetail courseDetail) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("COURSE_CODE", courseDetail.getCourseCode());
        values.put("STUDENT_CODE", courseDetail.getStudentCode());
        return db.insert("COURSE_DETAIL", null, values);
    }

    public int update(CourseDetail courseDetail) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("COURSE_CODE", courseDetail.getCourseCode());
        values.put("STUDENT_CODE", courseDetail.getStudentCode());
        return db.update("COURSE_DETAIL", values, "COURSE_DETAIL_CODE=?", new String[]{String.valueOf(courseDetail.getCourseDetailCode())});
    }

    public int delete(CourseDetail courseDetail) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("COURSE_DETAIL", "COURSE_DETAIL_CODE=?", new String[]{String.valueOf(courseDetail.getCourseDetailCode())});
    }
}
