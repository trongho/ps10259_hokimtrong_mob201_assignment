package com.example.ps10259_mob201_courses_manager_assignment.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ps10259_mob201_courses_manager_assignment.database.DBHelper;
import com.example.ps10259_mob201_courses_manager_assignment.model.CourseSchedule;
import com.example.ps10259_mob201_courses_manager_assignment.model.TestSchedule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TestScheduleDAO {
    DBHelper dbHelper;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

    public TestScheduleDAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    public ArrayList<TestSchedule> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<TestSchedule> list = new ArrayList<>();
        String sql = "SELECT * FROM TEST_SCHEDULE JOIN COURSE ON TEST_SCHEDULE.COURSE_CODE=COURSE.COURSE_CODE ";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int testScheduleCode = c.getInt(0);
                String courseCode = c.getString(1);
                String room = c.getString(2);
                Date testDate = simpleDateFormat.parse(c.getString(3));
                list.add(new TestSchedule(testScheduleCode, courseCode,room,testDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<TestSchedule> getTestScheduleByStudentCode(String sCode) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<TestSchedule> list = new ArrayList<>();
        String sql = "SELECT * FROM TEST_SCHEDULE JOIN COURSE_DETAIL ON TEST_SCHEDULE.COURSE_CODE=COURSE_DETAIL.COURSE_CODE WHERE COURSE_DETAIL.STUDENT_CODE=? ";
        Cursor c = db.rawQuery(sql, new String[]{sCode});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int testScheduleCode = c.getInt(0);
                String courseCode = c.getString(1);
                String room = c.getString(2);
                Date testDate = simpleDateFormat.parse(c.getString(3));
                list.add(new TestSchedule(testScheduleCode, courseCode,room,testDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public long insert(TestSchedule testSchedule) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("TEST_SCHEDULE_CODE", testSchedule.getTestScheduleCode());
        values.put("COURSE_CODE", testSchedule.getCourseCode());
        values.put("ROOM", testSchedule.getRoom());
        values.put("TEST_DATE", simpleDateFormat.format(testSchedule.getTestDate()));
        return db.insert("TEST_SCHEDULE", null, values);
    }

    public int update(TestSchedule testSchedule) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("TEST_SCHEDULE_CODE", testSchedule.getTestScheduleCode());
        values.put("COURSE_CODE", testSchedule.getCourseCode());
        values.put("ROOM", testSchedule.getRoom());
        values.put("TEST_DATE", simpleDateFormat.format(testSchedule.getTestDate()));
        return db.update("TEST_SCHEDULE", values, "TEST_SCHEDULE_CODE=?", new String[]{String.valueOf(testSchedule.getTestScheduleCode())});
    }

    public int delete(TestSchedule testSchedule) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("TEST_SCHEDULE", "TEST_SCHEDULE_CODE=?", new String[]{String.valueOf(testSchedule.getTestScheduleCode())});
    }
}
