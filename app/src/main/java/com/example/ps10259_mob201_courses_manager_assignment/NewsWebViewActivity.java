package com.example.ps10259_mob201_courses_manager_assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class NewsWebViewActivity extends AppCompatActivity {

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_web_view);
        setTitle("Web View");

        webView=findViewById(R.id.webView);

        webView.setWebViewClient(new WebViewClient(){
                                     @Override
                                     public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                         webView.loadUrl(url);
                                         return true;
                                     }
                                 }

        );

        //lấy intent
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("new");
        String link=bundle.getString("link");

        webView.loadUrl(link);
    }
}
