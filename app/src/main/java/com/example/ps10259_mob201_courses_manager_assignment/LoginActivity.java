package com.example.ps10259_mob201_courses_manager_assignment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ps10259_mob201_courses_manager_assignment.dao.StudentDAO;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

public class LoginActivity extends AppCompatActivity {
    TextInputEditText edtStudentCode, edtPassword;
    CheckBox chkSaveInfo;
    FloatingActionButton floatingActionButton;
    SharedPreferences storage;
    Button btnRegister;
    StudentDAO studentDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Login");

        initComponent();
        login();
        register();
    }


    public void initComponent(){
        edtStudentCode = findViewById(R.id.edtStudentCode);
        edtPassword = findViewById(R.id.edtPassword);
        chkSaveInfo = findViewById(R.id.chkSaveInfo);
        floatingActionButton = findViewById(R.id.fab);
        btnRegister=findViewById(R.id.btnRegister);
    }

    public void login(){
        setChkSaveInfo();
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLogin();
            }
        });
    }

    public void checkLogin(){
        String studentCode=edtStudentCode.getText().toString();
        String password=edtPassword.getText().toString();
        studentDAO=new StudentDAO(LoginActivity.this);
        if(studentDAO.checkLogin(studentCode,password)==true)
        {
            Toast.makeText(getApplicationContext(),"Login success!!!",Toast.LENGTH_SHORT).show();
            //kiểm tra checkbox đã check thì lưu dữ liệu đăng nhập
            storage = getSharedPreferences("myfile", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor=storage.edit();
            if(chkSaveInfo.isChecked())
            {

                editor.putString("student_code",studentCode);
                editor.putString("password",password);
            }
            editor.putBoolean("save_infomation",true);
            editor.commit();

            Intent intent=new Intent(LoginActivity.this,MainActivity.class);
            intent.putExtra("student_code", studentCode);
            startActivity(intent);
        }
        else
        {
            edtStudentCode.setText("");
            edtPassword.setText("");
            edtStudentCode.requestFocus();
        }
    }

    public void setChkSaveInfo(){
        //lưu usernamaevà mật khẩu
        storage = getSharedPreferences("myfile", Context.MODE_PRIVATE);

        //nạp thông tin lên form từ sharedPreference
        Boolean saveInfo = storage.getBoolean("save_infomation", false);
        if (saveInfo) {
            edtStudentCode.setText(storage.getString("student_code", ""));
            edtPassword.setText(storage.getString("password", ""));
            chkSaveInfo.setChecked(true);
        }
    }

    public void register(){
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });
    }
}
