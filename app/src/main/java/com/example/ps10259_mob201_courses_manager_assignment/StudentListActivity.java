package com.example.ps10259_mob201_courses_manager_assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.example.ps10259_mob201_courses_manager_assignment.adapter.StudentAdapter;
import com.example.ps10259_mob201_courses_manager_assignment.dao.StudentDAO;
import com.example.ps10259_mob201_courses_manager_assignment.model.Student;

import java.util.ArrayList;

public class StudentListActivity extends AppCompatActivity {
    ListView lvCourseList;
    Student student;
    ArrayList<Student> list;
    StudentDAO studentDAO;
    StudentAdapter studentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);
        setTitle("Student List");

        initComponent();
        shoStudentList();
    }

    public void initComponent(){
        lvCourseList=findViewById(R.id.lvStudentList);
    }

    public void shoStudentList(){
        student=new Student();
        list=new ArrayList<>();
        studentDAO=new StudentDAO(StudentListActivity.this);
        list=studentDAO.getAll();
        studentAdapter=new StudentAdapter(list,StudentListActivity.this);
        lvCourseList.setAdapter(studentAdapter);
    }


}
