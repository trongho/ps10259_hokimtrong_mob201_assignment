package com.example.ps10259_mob201_courses_manager_assignment.service;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

import com.example.ps10259_mob201_courses_manager_assignment.RegisterActivity;
import com.example.ps10259_mob201_courses_manager_assignment.dao.CourseDetailDAO;
import com.example.ps10259_mob201_courses_manager_assignment.fragment.MyCourseFragment;
import com.example.ps10259_mob201_courses_manager_assignment.model.CourseDetail;

public class CourseRegisterService extends Service {
    CourseDetailDAO courseDetailDAO;
    CourseDetail courseDetail;

    public CourseRegisterService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Bundle bundle = intent.getBundleExtra("register_course");
        String courseCode = bundle.getString("course_code");
        String studentCode = bundle.getString("student_code");

        Toast.makeText(this, "Course code: " + courseCode + "\n Student code: " + studentCode, Toast.LENGTH_SHORT).show();

        courseDetail = new CourseDetail();
        courseDetailDAO = new CourseDetailDAO(CourseRegisterService.this);
        courseDetail.setCourseCode(courseCode);
        courseDetail.setStudentCode(studentCode);

        if (courseDetailDAO.insert(courseDetail) == -1) {
            Toast.makeText(CourseRegisterService.this, "Insert fail!!!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(CourseRegisterService.this, "Insert success!!!", Toast.LENGTH_SHORT).show();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
