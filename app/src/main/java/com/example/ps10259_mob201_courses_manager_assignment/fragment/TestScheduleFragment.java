package com.example.ps10259_mob201_courses_manager_assignment.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.ps10259_mob201_courses_manager_assignment.R;
import com.example.ps10259_mob201_courses_manager_assignment.adapter.TestScheduleAdapter;
import com.example.ps10259_mob201_courses_manager_assignment.dao.TestScheduleDAO;
import com.example.ps10259_mob201_courses_manager_assignment.model.TestSchedule;
import com.example.ps10259_mob201_courses_manager_assignment.service.CourseScheduleService;
import com.example.ps10259_mob201_courses_manager_assignment.service.TestScheduleService;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TestScheduleFragment extends Fragment {
    ListView lvTestSchedule;
    TestSchedule testSchedule;
    ArrayList<TestSchedule> list;
    TestScheduleDAO testScheduleDAO;
    TestScheduleAdapter testScheduleAdapter;


    public TestScheduleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_test_schedule, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Courses");

        initComponent();
        showTestSchdule();
    }

    public void initComponent(){
        lvTestSchedule=getView().findViewById(R.id.lvTestSchedule);
    }

    public String getStudentCodeFromLogin(){
        Intent intent=getActivity().getIntent();
        String sCode=intent.getStringExtra("student_code");
        return  sCode;
    }

    public void showTestSchdule(){

        //Khởi tạo đối tượng intent
        Intent intent=new Intent(getContext(), TestScheduleService.class);
        //khởi tạo service
        getContext().startService(intent);

        testSchedule=new TestSchedule();
        list=new ArrayList<TestSchedule>();
        testScheduleDAO=new TestScheduleDAO(getContext());
        list=testScheduleDAO.getTestScheduleByStudentCode(getStudentCodeFromLogin());
        testScheduleAdapter=new TestScheduleAdapter(list,getContext());
        lvTestSchedule.setAdapter(testScheduleAdapter);
    }
}
