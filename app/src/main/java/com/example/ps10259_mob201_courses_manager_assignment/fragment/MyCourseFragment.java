package com.example.ps10259_mob201_courses_manager_assignment.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.ps10259_mob201_courses_manager_assignment.MainActivity;
import com.example.ps10259_mob201_courses_manager_assignment.R;
import com.example.ps10259_mob201_courses_manager_assignment.adapter.CourseDetailAdapter;
import com.example.ps10259_mob201_courses_manager_assignment.dao.CourseDetailDAO;
import com.example.ps10259_mob201_courses_manager_assignment.model.CourseDetail;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyCourseFragment extends Fragment {
    ListView lvMyCourseList;
    CourseDetail courseDetail;
    ArrayList<CourseDetail> list;
    CourseDetailDAO courseDetailDAO;
    CourseDetailAdapter courseDetailAdapter;

    public MyCourseFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_course, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Courses");

        initComponent();
        showMyCourseList();
    }

    public void initComponent(){
        lvMyCourseList=getView().findViewById(R.id.lvMyCourseList);
    }

    public String getStudentCodeFromLogin(){
        Intent intent=getActivity().getIntent();
        String sCode=intent.getStringExtra("student_code");
        return  sCode;
    }

    public void showMyCourseList(){
        courseDetail=new CourseDetail();
        list=new ArrayList<>();
        courseDetailDAO=new CourseDetailDAO(getContext());
        list=courseDetailDAO.getCourseByStudentCode(getStudentCodeFromLogin());
        courseDetailAdapter=new CourseDetailAdapter(list,getContext());
        lvMyCourseList.setAdapter(courseDetailAdapter);
    }
}
