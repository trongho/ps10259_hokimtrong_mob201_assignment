package com.example.ps10259_mob201_courses_manager_assignment.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.ps10259_mob201_courses_manager_assignment.EditStudentActivity;
import com.example.ps10259_mob201_courses_manager_assignment.R;
import com.example.ps10259_mob201_courses_manager_assignment.StudentListActivity;
import com.example.ps10259_mob201_courses_manager_assignment.dao.StudentDAO;
import com.example.ps10259_mob201_courses_manager_assignment.model.Student;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {
    TextView tvStudentCode,tvStudentName,tvPassword;
    Button btnEditStudent,btnStudentList;
    StudentDAO studentDAO;
    Student student;


    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Account");

        initComponent();
        getCurentStudent();
        setBtnStudentList();
        setBtnEditStudent();

    }

    public void initComponent(){
        tvStudentCode=getView().findViewById(R.id.tvStudentCode);
        tvStudentName=getView().findViewById(R.id.tvStudentName);
        tvPassword=getView().findViewById(R.id.tvPassword);
        btnStudentList=getView().findViewById(R.id.btnStudentList);
        btnEditStudent=getView().findViewById(R.id.btnEditStudent);
    }

    public void getCurentStudent(){
        //get student code form login activity
        Intent intent = getActivity().getIntent();
        final String studentCode = intent.getStringExtra("student_code");

        //get student from student code
        studentDAO=new StudentDAO(getContext());
        student=studentDAO.getStudentByID(studentCode);
        tvStudentCode.setText(student.getStudentCode());
        tvStudentName.setText(student.getStudentName());
        tvPassword.setText(student.getPassword());
    }

    public void setBtnStudentList(){
        btnStudentList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getContext(), StudentListActivity.class);
                startActivity(intent);
            }
        });
    }

    public void setBtnEditStudent(){
        btnEditStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getContext(), EditStudentActivity.class);
                Bundle bundle=new Bundle();
                bundle.putString("student_code",tvStudentCode.getText().toString());
                bundle.putString("student_name",tvStudentName.getText().toString());
                bundle.putString("password",tvPassword.getText().toString());
                intent.putExtra("edit_student",bundle);
                startActivity(intent);
            }
        });
    }

}
