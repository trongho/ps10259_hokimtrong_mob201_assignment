package com.example.ps10259_mob201_courses_manager_assignment.fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.ps10259_mob201_courses_manager_assignment.R;
import com.example.ps10259_mob201_courses_manager_assignment.adapter.CourseAdapter;
import com.example.ps10259_mob201_courses_manager_assignment.dao.CourseDAO;
import com.example.ps10259_mob201_courses_manager_assignment.model.Course;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CourseListFragment extends Fragment {
    ListView lvCourseList;
    Course course;
    ArrayList<Course> list;
    CourseDAO courseDAO;
    CourseAdapter courseAdapter;


    public CourseListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_course_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Courses");

        initComponent();
        showCourseList();

    }

    public void initComponent(){
        lvCourseList=getView().findViewById(R.id.lvCourseList);
    }

    public void showCourseList(){
        course=new Course();
        list=new ArrayList<>();
        courseDAO=new CourseDAO(getActivity());
        list=courseDAO.getAll();
        courseAdapter=new CourseAdapter(list,getActivity());
        lvCourseList.setAdapter(courseAdapter);
    }
}
