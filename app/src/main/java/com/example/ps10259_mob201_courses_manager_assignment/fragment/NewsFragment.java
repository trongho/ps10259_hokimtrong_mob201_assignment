package com.example.ps10259_mob201_courses_manager_assignment.fragment;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.ps10259_mob201_courses_manager_assignment.NewsReader;
import com.example.ps10259_mob201_courses_manager_assignment.NewsWebViewActivity;
import com.example.ps10259_mob201_courses_manager_assignment.R;
import com.example.ps10259_mob201_courses_manager_assignment.adapter.NewsAdapter;
import com.example.ps10259_mob201_courses_manager_assignment.model.News;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends Fragment {
    ListView lvNews;
    NewsAdapter newsAdapter;
    News news;
    List<News> newes;


    public NewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("News");

        initComponent();
        NewsSeedAsyncTask newsSeedAsyncTask = new NewsSeedAsyncTask();
        newsSeedAsyncTask.execute();
        itemNewsClick();
    }

    public void initComponent(){
        lvNews = getView().findViewById(R.id.lvNews);
    }

    class NewsSeedAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL("https://vnexpress.net/rss/giao-duc.rss");
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                newes = NewsReader.listNews(inputStream);
                newsAdapter = new NewsAdapter(getActivity(), newes);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lvNews.setAdapter(newsAdapter);

                    }
                });
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

    }

    public void itemNewsClick() {
        lvNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                news = newes.get(position);
                Intent intent = new Intent(getActivity(), NewsWebViewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("link", news.getLink());
                intent.putExtra("new", bundle);
                startActivity(intent);
            }
        });
    }
}
