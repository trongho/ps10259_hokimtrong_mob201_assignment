package com.example.ps10259_mob201_courses_manager_assignment.fragment;


import android.content.ContentUris;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.ps10259_mob201_courses_manager_assignment.MainActivity;
import com.example.ps10259_mob201_courses_manager_assignment.R;
import com.example.ps10259_mob201_courses_manager_assignment.adapter.CourseScheduleAdapter;
import com.example.ps10259_mob201_courses_manager_assignment.dao.CourseScheduleDAO;
import com.example.ps10259_mob201_courses_manager_assignment.model.CourseSchedule;
import com.example.ps10259_mob201_courses_manager_assignment.service.CourseRegisterService;
import com.example.ps10259_mob201_courses_manager_assignment.service.CourseScheduleService;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CourseScheduleFragment extends Fragment {
    ListView lvCourseSchedule;
    CourseSchedule courseSchedule;
    ArrayList<CourseSchedule> list;
    CourseScheduleDAO courseScheduleDAO;
    CourseScheduleAdapter courseScheduleAdapter;


    public CourseScheduleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_course_schedule, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Courses");

        initComponent();
        showCourseSchedule();
    }

    public void initComponent(){
        lvCourseSchedule=getView().findViewById(R.id.lvCourseSchedule);
    }


    public String getStudentCodeFromLogin(){
        Intent intent=getActivity().getIntent();
        String sCode=intent.getStringExtra("student_code");
        return  sCode;
    }

    public void showCourseSchedule(){

        //Khởi tạo đối tượng intent
        Intent intent=new Intent(getContext(), CourseScheduleService.class);
        //khởi tạo service
        getContext().startService(intent);


        courseSchedule=new CourseSchedule();
        list=new ArrayList<>();
        courseScheduleDAO=new CourseScheduleDAO(getActivity());
        list=courseScheduleDAO.getCourseScheduleByStudentCode(getStudentCodeFromLogin());
        courseScheduleAdapter=new CourseScheduleAdapter(list,getActivity());
        lvCourseSchedule.setAdapter(courseScheduleAdapter);

    }
}
