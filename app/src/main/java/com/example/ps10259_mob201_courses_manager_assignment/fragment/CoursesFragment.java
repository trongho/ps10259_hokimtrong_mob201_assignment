package com.example.ps10259_mob201_courses_manager_assignment.fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ps10259_mob201_courses_manager_assignment.R;
import com.example.ps10259_mob201_courses_manager_assignment.adapter.SectionsPagerAdapter;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class CoursesFragment extends Fragment {
    ViewPager viewPager;
    TabLayout tabLayout;
    SectionsPagerAdapter sectionsPagerAdapter;


    public CoursesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_courses, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Courses");

        initComponent();
        setViewPager();
        setTabLayout();

    }

    public void initComponent(){
        viewPager=getView().findViewById(R.id.viewpager);
        tabLayout=getView().findViewById(R.id.tabs);
    }

    public void setViewPager(){
        sectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());
        sectionsPagerAdapter.addFragment(new CourseListFragment());
        sectionsPagerAdapter.addFragment(new MyCourseFragment());
        sectionsPagerAdapter.addFragment(new CourseScheduleFragment());
        sectionsPagerAdapter.addFragment(new TestScheduleFragment());
        viewPager.setAdapter(sectionsPagerAdapter);
    }

    public void setTabLayout(){
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setText("Courses");
        tabLayout.getTabAt(1).setText("My course");
        tabLayout.getTabAt(2).setText("Schedule");
        tabLayout.getTabAt(3).setText("Test");
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
