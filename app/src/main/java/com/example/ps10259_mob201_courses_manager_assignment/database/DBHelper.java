package com.example.ps10259_mob201_courses_manager_assignment.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    public final static String DBNAME = "COURSES_MANAGER";
    public final static int DBVERSION = 1;

    public DBHelper(Context context) {
        super(context, DBNAME, null, DBVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql;
        //creat student table
        sql = "CREATE TABLE STUDENT(" +
                "STUDENT_CODE TEXT PRIMARY KEY," +
                "STUDENT_NAME TEXT NOT NULL ," +
                "PASSWORD TEXT NOT NULL)";
        db.execSQL(sql);

        //creat course table
        sql = "CREATE TABLE COURSE(" +
                "COURSE_CODE TEXT PRIMARY KEY," +
                "COURSE_NAME TEXT NOT NULL," +
                "TEACHER_NAME TEXT NOT NULL," +
                "CREDIT_NUMBER INTEGER NOT NULL," +
                "START_DATE DATE NOT NULL," +
                "END_DATE DATE NOT NULL)";
        db.execSQL(sql);

        //creat course detail table
        sql = "CREATE TABLE COURSE_DETAIL(" +
                "COURSE_DETAIL_CODE INTEGER PRIMARY KEY AUTOINCREMENT," +
                "COURSE_CODE TEXT NOT NULL REFERENCES COURSE(COURSE_CODE)," +
                "STUDENT_CODE TEXT NOT NULL REFERENCES STUDENT(STUDENT_CODE))";
        db.execSQL(sql);

        //creat course schedule table
        sql = "CREATE TABLE COURSE_SCHEDULE(" +
                "COURSE_SCHEDULE_CODE INTEGER PRIMARY KEY AUTOINCREMENT," +
                "COURSE_CODE TEXT REFERENCES COURSE(COURSE_CODE)," +
                "COURSE_CONTENT TEXT NOT NULL," +
                "ROOM TEXT NOT NULL," +
                "STUDY_DATE DATE NOT NULL)";
        db.execSQL(sql);

        //creat test schedule table
        sql = "CREATE TABLE TEST_SCHEDULE(" +
                "TEST_SCHEDULE_CODE INTEGER PRIMARY KEY AUTOINCREMENT," +
                "COURSE_CODE TEXT REFERENCES COURSE(COURSE_CODE)," +
                "ROOM TEXT NOT NULL," +
                "TEST_DATE DATE NOT NULL)";
        db.execSQL(sql);

        //thêm dữ liệu vào table student
        sql = "INSERT INTO STUDENT(STUDENT_CODE,STUDENT_NAME,PASSWORD)" +
                "VALUES('ps10259','Hồ Kim Trọng','123456')";
        db.execSQL(sql);

        //thêm dữ liệu vào table môn học
        sql = "INSERT INTO COURSE(COURSE_CODE,COURSE_NAME,TEACHER_NAME,CREDIT_NUMBER,START_DATE,END_DATE)" +
                "VALUES('MOB201','Android nâng cao','Hồ Đắc Hưng',3,'20-09-2019','30-11-2019')";
        db.execSQL(sql);
        sql = "INSERT INTO COURSE(COURSE_CODE,COURSE_NAME,TEACHER_NAME,CREDIT_NUMBER,START_DATE,END_DATE)" +
                "VALUES('MOB204','Dự án mẫu','Nguyễn Huỳnh Anh Kiệt',3,'20-09-2019','30-11-2019')";
        db.execSQL(sql);
        sql = "INSERT INTO COURSE(COURSE_CODE,COURSE_NAME,TEACHER_NAME,CREDIT_NUMBER,START_DATE,END_DATE)" +
                "VALUES('EN14302_3','Tiếng anh 2.2','Nguyễn Hiếu Lê Minh Hiền',3,'20-09-2019','19-1-2019')";
        db.execSQL(sql);

        //thêm dữ liệu vào table course schdule
        sql = "INSERT INTO COURSE_SCHEDULE(COURSE_CODE,COURSE_CONTENT,ROOM,STUDY_DATE)" +
                "VALUES('MOB201','Sử dụng service','T503','20-10-2019 07:30:00')";
        db.execSQL(sql);
        sql = "INSERT INTO COURSE_SCHEDULE(COURSE_CODE,COURSE_CONTENT,ROOM,STUDY_DATE)" +
                "VALUES('MOB201','Google map api','T503','28-10-2019 07:30:00')";
        db.execSQL(sql);
        sql = "INSERT INTO COURSE_SCHEDULE(COURSE_CODE,COURSE_CONTENT,ROOM,STUDY_DATE)" +
                "VALUES('MOB201','Broadcad receiver','T503','8-11-2019 07:30:00')";
        db.execSQL(sql);
        sql = "INSERT INTO COURSE_SCHEDULE(COURSE_CODE,COURSE_CONTENT,ROOM,STUDY_DATE)" +
                "VALUES('MOB204','Thiết kế database','T405','21-10-2019 07:30:00')";
        db.execSQL(sql);
        sql = "INSERT INTO COURSE_SCHEDULE(COURSE_CODE,COURSE_CONTENT,ROOM,STUDY_DATE)" +
                "VALUES('MOB204','Thiết kế model','T405','29-10-2019 07:30:00')";
        db.execSQL(sql);
        sql = "INSERT INTO COURSE_SCHEDULE(COURSE_CODE,COURSE_CONTENT,ROOM,STUDY_DATE)" +
                "VALUES('MOB204','Thiết kế DAO','T405','9-11-2019 07:30:00')";
        db.execSQL(sql);
        sql = "INSERT INTO COURSE_SCHEDULE(COURSE_CODE,COURSE_CONTENT,ROOM,STUDY_DATE)" +
                "VALUES('EN14302_3','Eating','T309','21-10-2019 09:45:00')";
        db.execSQL(sql);
        sql = "INSERT INTO COURSE_SCHEDULE(COURSE_CODE,COURSE_CONTENT,ROOM,STUDY_DATE)" +
                "VALUES('EN14302_3','The arts','T309','29-10-2019 09:45:00')";
        db.execSQL(sql);
        sql = "INSERT INTO COURSE_SCHEDULE(COURSE_CODE,COURSE_CONTENT,ROOM,STUDY_DATE)" +
                "VALUES('EN14302_3','Driving','T309','9-11-2019 09:45:00')";
        db.execSQL(sql);

        //thêm dữ liệu vào table test schdule
        sql = "INSERT INTO TEST_SCHEDULE(COURSE_CODE,ROOM,TEST_DATE)" +
                "VALUES('MOB201','T101','2-11-2019 7:30:00')";
        db.execSQL(sql);
        //thêm dữ liệu vào table test schdule
        sql = "INSERT INTO TEST_SCHEDULE(COURSE_CODE,ROOM,TEST_DATE)" +
                "VALUES('MOB204','T102','3-11-2019 7:30:00')";
        db.execSQL(sql);
        //thêm dữ liệu vào table test schdule
        sql = "INSERT INTO TEST_SCHEDULE(COURSE_CODE,ROOM,TEST_DATE)" +
                "VALUES('EN14302_3','T103','3-11-2019 7:30:00')";
        db.execSQL(sql);

        //thêm dữ liệu vào table course detail
        //sql = "INSERT INTO COURSE_DETAIL(COURSE_CODE,STUDENT_CODE)" +
       //         "VALUES('MOB201','ps10259')";
      //  db.execSQL(sql);
       // sql = "INSERT INTO COURSE_DETAIL(COURSE_CODE,STUDENT_CODE)" +
       //         "VALUES('MOB204','ps10259')";
       // db.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql;
        sql = "DROP TABLE IF EXISTS STUDENT";
        db.execSQL(sql);
        sql = "DROP TABLE IF EXISTS COURSE";
        db.execSQL(sql);
        sql = "DROP TABLE IF EXISTS COURSE_DETAIL";
        db.execSQL(sql);
        sql = "DROP TABLE IF EXISTS COURSE_SCHEDULE";
        db.execSQL(sql);
        sql = "DROP TABLE IF EXISTS TEST_SCHEDULE";
        db.execSQL(sql);
        onCreate(db);
    }
}
