package com.example.ps10259_mob201_courses_manager_assignment.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ps10259_mob201_courses_manager_assignment.R;
import com.example.ps10259_mob201_courses_manager_assignment.dao.TestScheduleDAO;
import com.example.ps10259_mob201_courses_manager_assignment.model.TestSchedule;

import java.text.SimpleDateFormat;
import java.util.List;

public class TestScheduleAdapter extends BaseAdapter {
    List<TestSchedule> list;
    public Context context;
    public LayoutInflater inflater;
    TestScheduleDAO testScheduleDAO;
    TestScheduleAdapter.ViewHolder holder;
    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

    public TestScheduleAdapter(List<TestSchedule> list, Context context) {
        super();
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        testScheduleDAO= new TestScheduleDAO(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final TestSchedule testSchedule= list.get(position);
        if (convertView == null) {
            holder = new TestScheduleAdapter.ViewHolder();
            convertView = inflater.inflate(R.layout.one_cell_test_schedule_layout, null);

            //ánh xạ
            holder.ivIcon= (ImageView) convertView.findViewById(R.id.ivIcon);
            holder.tvCourseCode= (TextView) convertView.findViewById(R.id.tvCourseCode);
            holder.tvRoom = (TextView) convertView.findViewById(R.id.tvRoom);
            holder.tvTestDate = (TextView) convertView.findViewById(R.id.tvTestDate);

            //set data lên layout custom
            holder.tvCourseCode.setText(testSchedule.getCourseCode());
            holder.tvRoom.setText(testSchedule.getRoom());
            holder.tvTestDate.setText(simpleDateFormat.format(testSchedule.getTestDate()));

            convertView.setTag(holder);
        } else
            holder = (TestScheduleAdapter.ViewHolder) convertView.getTag();

            ObjectAnimator animation = ObjectAnimator.ofFloat(convertView, "translationX", -768f, 0f);
            animation.setDuration(1000+position*1000);
            animation.start();


        return convertView;
    }

    public static class ViewHolder {
        ImageView ivIcon;
        TextView tvCourseCode;
        TextView tvRoom;
        TextView tvTestDate;
    }
}
