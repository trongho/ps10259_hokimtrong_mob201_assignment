package com.example.ps10259_mob201_courses_manager_assignment.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ps10259_mob201_courses_manager_assignment.R;
import com.example.ps10259_mob201_courses_manager_assignment.model.News;

import java.util.List;

public class NewsAdapter extends BaseAdapter {

    Context context;
    List<News> list;
    LayoutInflater layoutInflater;
    ViewHolder holder;

    public NewsAdapter(Context context, List<News> list) {
        this.context = context;
        this.list = list;
        layoutInflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public static class ViewHolder{
        ImageView ivIcon;
        TextView tvTitle;
        TextView tvLink;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        News news=list.get(position);
        if(convertView==null){
            holder=new ViewHolder();
            convertView=layoutInflater.inflate(R.layout.one_cell_news_layout,null);

            holder.ivIcon=(ImageView)convertView.findViewById(R.id.ivIcon);
            holder.tvTitle=(TextView)convertView.findViewById(R.id.tvTitle);
            holder.tvLink=(TextView)convertView.findViewById(R.id.tvLink);

            convertView.setTag(holder);
        }
        else
            holder=(ViewHolder)convertView.getTag();

        holder.tvTitle.setText(news.getTitle());
        holder.tvLink.setText(news.getLink());

        if(position%2==0) {
            ObjectAnimator animation = ObjectAnimator.ofFloat(convertView, "translationX", -768f, 0f);
            animation.setDuration(2000);
            animation.start();
        }
        else {
            ObjectAnimator animation2 = ObjectAnimator.ofFloat(convertView, "translationX", 768f, 0f);
            animation2.setDuration(2000);
            animation2.start();
        }

        return convertView;
    }
}
