package com.example.ps10259_mob201_courses_manager_assignment.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ps10259_mob201_courses_manager_assignment.MainActivity;
import com.example.ps10259_mob201_courses_manager_assignment.R;
import com.example.ps10259_mob201_courses_manager_assignment.dao.CourseDAO;
import com.example.ps10259_mob201_courses_manager_assignment.dao.CourseDetailDAO;
import com.example.ps10259_mob201_courses_manager_assignment.model.Course;
import com.example.ps10259_mob201_courses_manager_assignment.model.CourseDetail;

import java.text.SimpleDateFormat;
import java.util.List;

public class CourseDetailAdapter extends BaseAdapter {
    List<CourseDetail> list;
    public Context context;
    public LayoutInflater inflater;
    CourseDetailDAO courseDetailDAO;
    CourseDetailAdapter.ViewHolder holder;

    public CourseDetailAdapter(List<CourseDetail> list, Context context) {
        super();
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        courseDetailDAO= new CourseDetailDAO(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final CourseDetail courseDetail = list.get(position);
        if (convertView == null) {
            holder = new CourseDetailAdapter.ViewHolder();
            convertView = inflater.inflate(R.layout.one_cell_course_detail_layout, null);

            //ánh xạ
            holder.ivIcon= (ImageView) convertView.findViewById(R.id.ivIcon);
            holder.tvCourseCode= (TextView) convertView.findViewById(R.id.tvCourseCode);
            holder.tvStudentCode= (TextView) convertView.findViewById(R.id.tvStudentCode);
            holder.ivDelete= (ImageView) convertView.findViewById(R.id.ivDelete);

            //set data lên layout custom
            holder.tvCourseCode.setText(courseDetail.getCourseCode());
            holder.tvStudentCode.setText(courseDetail.getStudentCode());
            convertView.setTag(holder);
        } else
            holder = (CourseDetailAdapter.ViewHolder) convertView.getTag();


        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                courseDetailDAO=new CourseDetailDAO(context);
                courseDetailDAO.delete(courseDetail);
                capNhatLV();
            }
        });

        ObjectAnimator animation = ObjectAnimator.ofFloat(convertView, "translationY", -1280f,0f);
        animation.setDuration(1000+position*1000);
        animation.start();

        return convertView;
    }

    public static class ViewHolder {
        ImageView ivIcon;
        TextView tvCourseCode;
        TextView tvStudentCode;
        ImageView ivDelete;
    }

    public void capNhatLV(){
        list.clear();
        list.addAll(courseDetailDAO.getCourseByStudentCode(((MainActivity)context).getStudentCodeFromLogin()));
        this.notifyDataSetChanged();
    }
}
