package com.example.ps10259_mob201_courses_manager_assignment.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ps10259_mob201_courses_manager_assignment.R;
import com.example.ps10259_mob201_courses_manager_assignment.dao.StudentDAO;
import com.example.ps10259_mob201_courses_manager_assignment.model.Student;

import java.util.List;

public class StudentAdapter extends BaseAdapter {
    List<Student> list;
    public Context context;
    public LayoutInflater inflater;
    StudentDAO studentDAO;
    StudentAdapter.ViewHolder holder;

    public StudentAdapter(List<Student> list, Context context) {
        super();
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        studentDAO= new StudentDAO(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Student student = list.get(position);
        if (convertView == null) {
            holder = new StudentAdapter.ViewHolder();
            convertView = inflater.inflate(R.layout.one_cell_student_layout, null);

            //ánh xạ
            holder.ivIcon= (ImageView) convertView.findViewById(R.id.ivIcon);
            holder.tvStudentCode= (TextView) convertView.findViewById(R.id.tvStudentCode);
            holder.tvStudentName = (TextView) convertView.findViewById(R.id.tvStudentName);
            holder.ivDelete= (ImageView) convertView.findViewById(R.id.ivDelete);

            //set data lên layout custom
            holder.tvStudentCode.setText(student.getStudentCode());
            holder.tvStudentName.setText(student.getStudentName());
            convertView.setTag(holder);
        } else
            holder = (StudentAdapter.ViewHolder) convertView.getTag();


        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(studentDAO.delete(student)==-1){
                    Toast.makeText(context,"Delete fail!!!",Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(context,"Delete success!!!",Toast.LENGTH_SHORT).show();
                updateLV();
            }
        });

        if(position%2==0) {
            ObjectAnimator animation = ObjectAnimator.ofFloat(convertView, "translationX", -768f, 0f);
            animation.setDuration(2000);
            animation.start();
        }
        else {
            ObjectAnimator animation2 = ObjectAnimator.ofFloat(convertView, "translationX", 768f, 0f);
            animation2.setDuration(2000);
            animation2.start();
        }


        return convertView;
    }

    public static class ViewHolder {
        ImageView ivIcon;
        TextView tvStudentCode;
        TextView tvStudentName;
        ImageView ivDelete;
    }

    public void updateLV(){
        list.clear();
        list.addAll(studentDAO.getAll());
        this.notifyDataSetChanged();
    }
}
