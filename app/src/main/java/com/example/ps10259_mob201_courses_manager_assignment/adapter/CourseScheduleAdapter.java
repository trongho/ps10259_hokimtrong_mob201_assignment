package com.example.ps10259_mob201_courses_manager_assignment.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ps10259_mob201_courses_manager_assignment.R;
import com.example.ps10259_mob201_courses_manager_assignment.dao.CourseScheduleDAO;
import com.example.ps10259_mob201_courses_manager_assignment.model.CourseSchedule;
import com.example.ps10259_mob201_courses_manager_assignment.service.CourseScheduleService;

import java.text.SimpleDateFormat;
import java.util.List;

public class CourseScheduleAdapter extends BaseAdapter {
    List<CourseSchedule> list;
    public Context context;
    public LayoutInflater inflater;
    CourseScheduleDAO courseScheduleDAO;
    CourseScheduleAdapter.ViewHolder holder;
    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

    public CourseScheduleAdapter(List<CourseSchedule> list, Context context) {
        super();
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        courseScheduleDAO= new CourseScheduleDAO(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final CourseSchedule courseSchedule= list.get(position);
        if (convertView == null) {
            holder = new CourseScheduleAdapter.ViewHolder();
            convertView = inflater.inflate(R.layout.one_cell_course_schedule_layout, null);

            //ánh xạ
            holder.ivIcon= (ImageView) convertView.findViewById(R.id.ivIcon);
            holder.tvCourseCode= (TextView) convertView.findViewById(R.id.tvCourseCode);
            holder.tvCourseContent = (TextView) convertView.findViewById(R.id.tvContent);
            holder.tvRoom = (TextView) convertView.findViewById(R.id.tvRoom);
            holder.tvStudyDate = (TextView) convertView.findViewById(R.id.tvStudyDate);

            //set data lên layout custom
            holder.tvCourseCode.setText(courseSchedule.getCourseCode());
            holder.tvCourseContent.setText(courseSchedule.getCourseContent());
            holder.tvRoom.setText(courseSchedule.getRoom());
            holder.tvStudyDate.setText(simpleDateFormat.format(courseSchedule.getStudyDate()));

            convertView.setTag(holder);
        } else
            holder = (CourseScheduleAdapter.ViewHolder) convertView.getTag();


        return convertView;
    }

    public static class ViewHolder {
        ImageView ivIcon;
        TextView tvCourseCode;
        TextView tvCourseContent;
        TextView tvRoom;
        TextView tvStudyDate;
    }
}
