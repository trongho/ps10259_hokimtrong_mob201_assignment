package com.example.ps10259_mob201_courses_manager_assignment.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ps10259_mob201_courses_manager_assignment.MainActivity;
import com.example.ps10259_mob201_courses_manager_assignment.R;
import com.example.ps10259_mob201_courses_manager_assignment.dao.CourseDAO;
import com.example.ps10259_mob201_courses_manager_assignment.model.Course;
import com.example.ps10259_mob201_courses_manager_assignment.service.CourseRegisterService;

import java.text.SimpleDateFormat;
import java.util.List;

public class CourseAdapter extends BaseAdapter {
    List<Course> list;
    public Context context;
    public LayoutInflater inflater;
    CourseDAO courseDAO;
    CourseAdapter.ViewHolder holder;
    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd-MM-yyyy");

    public CourseAdapter(List<Course> list, Context context) {
        super();
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        courseDAO= new CourseDAO(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Course course= list.get(position);
        if (convertView == null) {
            holder = new CourseAdapter.ViewHolder();
            convertView = inflater.inflate(R.layout.one_cell_course_layout, null);

            //ánh xạ
            holder.ivIcon= (ImageView) convertView.findViewById(R.id.ivIcon);
            holder.tvCourseName= (TextView) convertView.findViewById(R.id.tvCourseName);
            holder.tvCreditNumber = (TextView) convertView.findViewById(R.id.tvCreditNumber);
            holder.tvTeacherName = (TextView) convertView.findViewById(R.id.tvTeacherName);
            holder.tvStartDate = (TextView) convertView.findViewById(R.id.tvStartDate);
            holder.tvEndDate = (TextView) convertView.findViewById(R.id.tvEndDate);
            holder.btnAdd= (Button) convertView.findViewById(R.id.btnAdd);

            //set data lên layout custom
            holder.tvCourseName.setText(course.getCourseName());
            holder.tvCreditNumber.setText(course.getCreditNumber()+"");
            holder.tvTeacherName.setText(course.getTeacherName());
            holder.tvStartDate.setText(simpleDateFormat.format(course.getStartDate()));
            holder.tvEndDate.setText(simpleDateFormat.format(course.getEndDate()));

            convertView.setTag(holder);
        } else
            holder = (CourseAdapter.ViewHolder) convertView.getTag();

        //thêm môn học vào môn học của tôi
        holder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Khởi tạo đối tượng intent và bundle
                Intent intent=new Intent(context, CourseRegisterService.class);
                Bundle bundle=new Bundle();
                //khai báo các bộ giá trị và gán vào bundle
                bundle.putString("course_code",course.getCourseCode());
                bundle.putString("student_code",((MainActivity)context).getStudentCodeFromLogin());
                //gắn bundle vào intent
                intent.putExtra("register_course",bundle);
                //khởi tạo service
                context.startService(intent);
            }
        });

        ObjectAnimator animation = ObjectAnimator.ofFloat(convertView, "translationX", -768f, 0f);
        animation.setDuration(1000+position*1000);
        animation.start();

        return convertView;
    }

    public static class ViewHolder {
        ImageView ivIcon;
        TextView tvCourseName;
        TextView tvCreditNumber;
        TextView tvTeacherName;
        TextView tvStartDate;
        TextView tvEndDate;
        Button btnAdd;
    }
}
