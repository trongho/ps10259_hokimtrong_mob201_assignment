package com.example.ps10259_mob201_courses_manager_assignment;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.ps10259_mob201_courses_manager_assignment.dao.StudentDAO;
import com.example.ps10259_mob201_courses_manager_assignment.fragment.MyCourseFragment;
import com.example.ps10259_mob201_courses_manager_assignment.model.Student;
import com.google.android.material.textfield.TextInputEditText;

public class RegisterActivity extends AppCompatActivity {
    TextInputEditText edtStudentCode, edtStudentName, edtPassword, edtRePassword;
    Button btnSignUp, btnCancel;
    Student student;
    StudentDAO studentDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setTitle("Register student");

        initComponent();
        register();
        cancelSignUp();
    }

    public void initComponent() {
        btnSignUp = findViewById(R.id.btnSignUp);
        btnCancel = findViewById(R.id.btnCancelSignUp);
        edtStudentCode = findViewById(R.id.edtStudentCode);
        edtPassword = findViewById(R.id.edtPassword);
        edtRePassword = findViewById(R.id.edtRePassword);
        edtStudentName = findViewById(R.id.edtStudentName);
    }

    public void register() {
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()>0) {
                    student = new Student();
                    student.setStudentCode(edtStudentCode.getText().toString());
                    student.setStudentName(edtStudentName.getText().toString());
                    student.setPassword(edtPassword.getText().toString());

                    studentDAO = new StudentDAO(RegisterActivity.this);
                    if (studentDAO.insert(student) == -1) {
                        Toast.makeText(RegisterActivity.this, "Insert fail!!!", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        Toast.makeText(RegisterActivity.this, "Insert success!!!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public int validate(){
        int check=1;
        //validate data
        if (edtStudentCode.length() == 0) {
            Toast.makeText(RegisterActivity.this, "Student code is not empty!!!", Toast.LENGTH_SHORT).show();
            check=-1;
        }
        if (edtStudentName.length() == 0) {
            Toast.makeText(RegisterActivity.this, "Student name is not empty!!!", Toast.LENGTH_SHORT).show();
            check=-1;
        }
        if (edtPassword.length() == 0) {
            Toast.makeText(RegisterActivity.this, "Password is not empty!!!", Toast.LENGTH_SHORT).show();
            check=-1;
        }
        if (edtRePassword.length() == 0) {
            Toast.makeText(RegisterActivity.this, "RePassword is not empty!!!", Toast.LENGTH_SHORT).show();
            check=-1;
        }
        if (!edtRePassword.getText().toString().equals(edtPassword.getText().toString())) {
            Toast.makeText(RegisterActivity.this, "RePassword is equals password!!!", Toast.LENGTH_SHORT).show();
            check=-1;
        }
        return check;
    }

    //thoát đăng ký
    public void cancelSignUp() {
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
