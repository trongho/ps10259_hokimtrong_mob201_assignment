package com.example.ps10259_mob201_courses_manager_assignment;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.ps10259_mob201_courses_manager_assignment.fragment.AboutFragment;
import com.example.ps10259_mob201_courses_manager_assignment.fragment.AccountFragment;
import com.example.ps10259_mob201_courses_manager_assignment.fragment.CourseListFragment;
import com.example.ps10259_mob201_courses_manager_assignment.fragment.CoursesFragment;
import com.example.ps10259_mob201_courses_manager_assignment.fragment.MyCourseFragment;
import com.example.ps10259_mob201_courses_manager_assignment.fragment.NewsFragment;
import com.example.ps10259_mob201_courses_manager_assignment.fragment.SocialFragment;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    FragmentManager fragmentManager=getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        //load home fragment
        fragmentManager.beginTransaction().add(R.id.frame_layout,new CoursesFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent=new Intent(MainActivity.this,MapsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_courses) {
            fragmentManager.beginTransaction().replace(R.id.frame_layout,new CoursesFragment()).commit();
        } else if (id == R.id.nav_news) {
            fragmentManager.beginTransaction().replace(R.id.frame_layout,new NewsFragment()).commit();
        } else if (id == R.id.nav_social) {
            fragmentManager.beginTransaction().replace(R.id.frame_layout,new SocialFragment()).commit();
        } else if (id == R.id.nav_account) {
            fragmentManager.beginTransaction().replace(R.id.frame_layout,new AccountFragment()).commit();
        } else if (id == R.id.nav_about) {
            fragmentManager.beginTransaction().replace(R.id.frame_layout,new AboutFragment()).commit();
        } else if (id == R.id.nav_exit) {
            finish();
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public String getStudentCodeFromLogin(){
        Intent intent=getIntent();
        String sCode=intent.getStringExtra("student_code");
        return  sCode;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
